package com.gov.usa.ssn.model;

import lombok.Data;

@Data	
public class SsnProviderModel {
	
	private int ssnId;
	
	private String firstName;
	
	private String lastName;
	
	private String dob;

	public String getLastName() {
		// TODO Auto-generated method stub
		return lastName;
	}

	public String getFirstName() {
		// TODO Auto-generated method stub
		return firstName;
	}

}
