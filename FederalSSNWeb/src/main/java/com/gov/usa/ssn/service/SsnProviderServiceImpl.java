package com.gov.usa.ssn.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gov.usa.ssn.dao.SsnProviderDao;
import com.gov.usa.ssn.entity.SsnProviderEntity;
import com.gov.usa.ssn.model.SsnProviderModel;

@Service("ssnService")
public class SsnProviderServiceImpl implements SsnProviderService {

	Logger logger= LoggerFactory.getLogger(SsnProviderServiceImpl.class);
	@Autowired(required = true)
	private SsnProviderDao providerDao;
	
	/**
	 * This method is used to save ssn number into the DB
	 */
	@Override
	public String saveSsnNo(SsnProviderModel spm) {
		
		logger.debug("saveSSNo. method started");
		
		SsnProviderEntity entity = null;
		Integer result = null;
		entity = new SsnProviderEntity();
		BeanUtils.copyProperties(spm, entity);
		result = providerDao.save(entity).getSsnId();
		
		logger.debug("SSNo. saved successfully");
		
		if (result != null)
			return "SSN Registration Done";
		else
			return "Failed";
		
	}
	/**
	 * This method is used to find ssn number and Date of Birth existence in DB
	 */
	@Override
	public SsnProviderModel findBySsnAndDob(Integer ssn, String dob) {
		SsnProviderModel model = null;
		
		logger.debug("finding SSNoAndDob existence started");
		
		// Getting data from DB
		SsnProviderEntity entity = providerDao.findBySsnIdAndDob(ssn, dob);

		if (entity != null) {
			// Copying Entity data to model
			model = new SsnProviderModel();
			BeanUtils.copyProperties(entity, model);
		}
		return model;
	}

}
