package com.gov.usa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FederalSsnWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(FederalSsnWebApplication.class, args);
	}

}

