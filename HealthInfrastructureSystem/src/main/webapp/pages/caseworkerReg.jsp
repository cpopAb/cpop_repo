<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>
    <%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>CaseWorker Registration</title>
 
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script
	src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script>
  
  //form validations
	$(function() {
		
		$('form[id="regUserForm"]').validate({
			rules : {
				firstName : 'required',
				lastName : 'required',
				email : {
					required : true,
					email : true,
				},
				pwd : {
					required : true,
					minlength : 5,
				},
				dob:'required',
				role:'required',
				phonenumber:'required'
			},
			messages : {
				firstName : 'Please enter first name',
				lastName : 'please enter last name',
				email : 'Please enter a valid email',
				pwd : {
					required :'Please enter password',
					minlength : 'Password must be at least 5 characters long'
				},
				dob:'Please select dob',
				role:'Please select a role',
				phonenumber:'Please enter Phno'
			},
			submitHandler : function(form) {
				form.submit();
			}
		});
		
		$( "#dob" ).datepicker({
			changeMonth : true,
			changeYear : true,
	    	dateFormat: "dd-mm-yyyy",
	    	maxDate:new Date()
	    });
		
		$("#txtemail").blur(function(){
			  var enteredEmail=$("#txtemail").val();
			  $.ajax({
				url:"/checkEmail",
				data:"email="+enteredEmail,
				success:function(result){
					if(result=='DUPLICATE'){
						$("#emailMsg").html("Email already registered!!");
						$("#txtemail").focus();
					}else{
						$("#emailMsg").html("");
					}
			  }
			});
		 });
	});
  </script>
</head>
</head>
<body>
<jsp:include page="header-inner.jsp"/>
<h1>CaseWorker Registration </h1>
	<form:form action="registerUser" modelAttribute="um" method="POST" id="regUserForm" name="regUserForm">
		<table>
			<tr>
				<td>First Name</td>
				<td>
					<form:input path="firstname" id="txtfname"></form:input>
				</td>
			</tr>
			<tr>
				<td>Last Name</td>
				<td>
					<form:input path="lastname" id="txtlname"></form:input>
				</td>
			</tr>
			<tr>
				<td>Email</td>
				<td>
					<form:input path="email" id="txtemail"></form:input><font color="red"><span id="emailMsg"></span></font>
				</td>
			</tr>
			<tr>
				<td>Phone Number</td>
				<td>
					<form:input path="phonenumber" id="txtphno"></form:input>
				</td>
			</tr>
			<tr>
				<td>Role</td>
				<td>
					<form:select items="${roles}" path="role" id="txtrole">
					</form:select>
				</td>
			</tr>
			<tr>
				<td>Date Of Birth</td>
				<td>
					<form:input path="dob" id="dob" readonly="true"></form:input>
				</td>
			</tr>
			<tr>
				<td>Password</td>
				<td>
					<form:input path="pwd" id="txtpwd"></form:input>
				</td>
			</tr>
		</table>
				<input type="submit" value="Register" id="txtsubmitBtn">&nbsp;&nbsp;
				<input type="reset" value="Reset" >
	</form:form>
	<font color="green">${success}</font>
	<font color="red">${error}</font>

</body>
</html>