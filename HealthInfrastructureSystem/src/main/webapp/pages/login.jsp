<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>CaseWorker Login</title>
</head>
<body>
	<jsp:include page="header-inner.jsp"/>
	<h1 style="text-align: center;">CaseWorker Login</h1>
<div>
	${error}
	<form:form action="login" method="POST" modelAttribute="um" name="loginForm" >
		<table>
			<tr>
				<td>Email</td>
				<td><form:input path="email" id="txtemail"/><span id="emailMsg"></span></td>
			</tr>
			<tr>
				<td>Password</td>
				<td><form:password path="pwd"/></td>
			</tr>
			<tr>
				<td><input type="submit" value="login" id="loginbtn"></td>
			</tr>
		</table>
	</form:form>
	<div>
		Not Registered? <a href="registerUser">Register here</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<a href="forgotPwd">forgot password?</a>
	</div>
</div>
</body>
</html>