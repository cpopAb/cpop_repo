<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>Add Individuals</title>

	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	
	
			<script>
			jQuery(document).ready(function($) {
			 $("#ssn").blur(function () {
				// Disble the search button
				enableSearchButton(true);
				
				// Prevent the form from submitting via the browser.
				event.preventDefault();
				searchViaAjax();
			});
		});
		function searchViaAjax() {
			var enteredDob=$("#dob").val();
			var enteredSsn = $("#ssn").val();
			var enteredFname=$("#firstname").val();
			$.ajax({
				type : "POST",
				contentType : "application/json",
				url : "verifySsn",
				data: JSON.stringify({
					"ssn":enteredSsn,
					"dob":enteredDob,
					"firstname":enteredFname
					}),
				dataType : 'text',
				success : function(data) {
					console.log("SUCCESS: ", data);
					if(data=="VALID"){
						enableSearchButton(false);
						$("#ssnMsg").html("Valid SSN");
					}else{
						$("#ssnMsg").html("Invalid SSN");
						$("#ssn").focus();
					}
				},
				error : function(e) {
					console.log("ERROR: ", e);
				}
			});
		}
		function enableSearchButton(flag) {
			$("#submitBtn").prop("disabled", flag);
		}
		/*$(function() { 
		$('form[id="indvRegForm"]').validate({
			rules : {
				firstname : 'required',
				lastname : 'required',
				dob:'required',
				ssn:'required',
				gender:'required'
			},
			messages : {
				firstname : 'Please enter first name',
				lastname : 'please enter last name',
				dob:'please select dob',
				gender:'please select gender',
				ssn:'please enter SSN'
			},
			submitHandler : function(form) {
				form.submit();
			}
		});
	
	
		$("#dob").datepicker({
			changeMonth : true,
			changeYear : true,
			dateFormat : 'dd/mm/yyyy',
			maxDate : new Date()
		});
		}); */ 
	</script>

</head>
<body>
	<h1>Add Individuals</h1>
	<div>
		<font color="green">${register}</font>
		<font color="red">${error}</font>
		<form:form action="addIndv" modelAttribute="indvRegModel" method="POST" id="indvRegForm">
			<div>
				<form:hidden path="caseNo"/>
				
				<label>First Name</label>
				<div>
					<form:input path="firstname" id="firstname"/>
				</div>
				<label>Last Name</label>
				<div>
					<form:input path="lastname" id="lastname"/>
				</div>
				
				<label>Date Of Birth</label>
				<div>
					<form:input path="dob" id="dob"/>
				</div>
				
				<label>SSN</label>
				<div>
					<form:input path="ssn" id="ssn" /> <span id="ssnMsg"></span>
				</div>
				
				<label>Gender</label>
				<div>
					<form:input path="gender" id="gender"/>
				</div>
			</div>
			<div>
				<input type="submit" value="Register" id="submitBtn" >
				<input type="reset" value="Reset" >
			</div>
		</form:form>
	</div>
</body>
</html>