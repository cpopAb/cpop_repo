<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>Forgot Password</title>
</head>
<body>
	<h2 style="color: blue;text-align: center;">Change Password</h2>
	<div>
		<form:form method="POST" modelAttribute="" action=""  id=""  name="" >
			<fieldset>
				<legend>Change Password</legend>
				<dl>
					<dt>Username</dt>
					<dd>
						<form:input path="username" name="ftxtuname" id="ftxtuname"/><span id="ajaxmsg"></span>
						
						<form:errors path="username"></form:errors>
						
					</dd>
					<dt>New Password</dt>
					<dd>
						<form:input path="password" name="ftxtpwd" id="ftxtpwd"/>
						<form:errors path="password"></form:errors>
					</dd>
					<dt>Confirm Password</dt>
					<dd>
						<form:input path="confirmPassword" name="ftxtcpwd" id="ftxtcpwd" />
						<form:errors path="confirmPassword"></form:errors>
					</dd>
				</dl>
				<input type="submit" value="Confirm" id="confirmbtn">
			</fieldset>
		</form:form>
	</div>
	<span style="text-align: center; color: blue;">${result}</span>
</body>
</html>