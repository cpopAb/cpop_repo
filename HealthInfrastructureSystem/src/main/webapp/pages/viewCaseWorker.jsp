	<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Case Workers</title>
	<script>
	function confirmDelete(){
		var status=confirm("Are you sure you want delete the case worker?");
		if(status)
			return true;
		else
			return false;
		
	}
	function confirmActiavte(){
		var status=confirm("Are you sure you want activate the case worker?");
		if(status)
			return true;
		else
			return false;
		
	}
	</script>
</head>
<body>
	<jsp:include page="header-inner.jsp"/>
	<h1>CaseWorkers</h1>
	<br>
	<div>
	<table border="1">
		<thead>
				<tr>
					<th>Sl No.</th>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Email</th>
					<th>Action</th>
				</tr>
		</thead>
		<tbody>
			<c:forEach items="${caseWorkers}" var="cw" varStatus="index">
				<tr>
					<td><c:out value="${index.count}"/></td>
					<td><c:out value="${cw.firstname}"/></td>
					<td><c:out value="${cw.lastname}"/></td>
					<td><c:out value="${cw.email}"/></td>
					<td>
						<a href="editCaseWorker?uid=${cw.userId}" >Edit</a>
						/
						<c:if test="${cw.activeSwitch=='Y'}">
							<a href="deleteCaseWorker?uid=${cw.userId}" onclick="return confirmDelete()">Delete</a>
						</c:if>
						<c:if test="${cw.activeSwitch=='N'}">
							<a href="activeCaseWorker?uid=${cw.userId}" onclick="return confirmActiavte()">Activate</a>
						</c:if>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	</div>
	<div>
	<table>
		<tr>
			<c:if test="${cpn ne 1}">
					<td><a href="viewCaseWorker?cpn=${cpn-1}">Previous</a></td>
			</c:if>
			<c:if test="${cpn == 1}">
					<td colspan="1"> </td>
			</c:if>
			<c:forEach begin="1" end="${totalPages}" var="thisPage">
				<c:choose>
					<c:when test="${thisPage==cpn}"><td>${thisPage}</td></c:when>
					<c:otherwise><td><a href="viewCaseWorker?cpn=${thisPage}">${thisPage}</a></td></c:otherwise>
				</c:choose>
			</c:forEach>
			<c:if test="${cpn ne totalPages}">
				<td><a href="viewCaseWorker?cpn=${cpn+1}">Next</a></td>
			</c:if>		
		</tr>
	</table>
	</div>
</body>
</html>