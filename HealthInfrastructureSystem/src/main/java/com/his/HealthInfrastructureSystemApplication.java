package com.his;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HealthInfrastructureSystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(HealthInfrastructureSystemApplication.class, args);
	}

}

