package com.his.dc.dao;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.his.dc.entity.IndividualRegistrationEntity;

@Repository("indvRegistrationDao")
public interface IndividualRegistrationDao extends JpaRepository<IndividualRegistrationEntity, Serializable> {

}
