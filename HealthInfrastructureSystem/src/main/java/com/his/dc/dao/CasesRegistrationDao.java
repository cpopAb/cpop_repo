package com.his.dc.dao;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.his.dc.entity.DcCasesEntity;

@Repository("caseRegistrationDao")
public interface CasesRegistrationDao extends JpaRepository<DcCasesEntity, Serializable> {

	@Query("from DcCasesEntity where email=:email")
	public DcCasesEntity findByEmail(String email);
}
