package com.his.dc.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.his.dc.entity.IndividualRegistrationEntity;
import com.his.dc.model.IndividualRegistrationModel;
import com.his.dc.service.IndividualRegistrationService;
import com.his.util.ARConstants;

@Controller
@SessionAttributes({"email","caseNo"})
public class DCController {
	
	@Autowired(required=true)
	private IndividualRegistrationService indvService;

	/**
	 * show individual registration form
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/addIndv",method=RequestMethod.GET)
	public String viewIndvRegForm(Model model) {
		if(model.containsAttribute("email")) {//checking if logged in or not. If logged in email is stored in session attribute and is checked. If not stored then display login page
			IndividualRegistrationModel indvRegModel=new IndividualRegistrationModel();
			model.addAttribute("indvRegModel", indvRegModel );
			return "indvReg";
		}
		else return "redirect:index";
	}
	/**
	 * this method is called on indvRegistration form submsssion
	 * @param email
	 * @param indvRegModel
	 * @param indvModel
	 * @return
	 */
	//in the method param @RequestParam("email") String email,we are getting email from session attribute 
	@RequestMapping(value="/addIndv",method=RequestMethod.POST)
	public String submitIndvRegForm(@ModelAttribute("email") String email, @ModelAttribute("indvRegModel") IndividualRegistrationModel indvRegModel, Model indvModel) {
	
		
			Integer caseNo=indvService.getCaseNo(email);
			indvRegModel.setCaseNo(caseNo);
			
			//adding caseNo to session attribute for any future use
			indvModel.addAttribute("caseNo", caseNo);
			
			IndividualRegistrationEntity entity=indvService.registerIndiv(indvRegModel);
			if(entity!=null) {
				indvModel.addAttribute(ARConstants.DC_REGISTER,ARConstants.DC_REGISTSER_MSG);
				return "indvReg";
			}else {
				indvModel.addAttribute(ARConstants.DC_NOT_REGISTSER,ARConstants.DC_NOT_REGISTSER_MSG);
				return "indvReg";
			}
	
	}
	
	/**
	 * method to verify ssn and dob using ajax call
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/verifySsn", method=RequestMethod.POST)
	public @ResponseBody String verifySsn(@RequestBody Map<String,String> indvDetails) {
		
		IndividualRegistrationModel indvRegModel=new IndividualRegistrationModel();
		indvRegModel.setFirstname(indvDetails.get("firstname"));
		indvRegModel.setDob(indvDetails.get("dob"));
		indvRegModel.setSsn(indvDetails.get("ssn"));
		boolean result=indvService.verifySsn(indvRegModel);
		System.out.println(result);
		if(result)
			return ARConstants.SUCCESS_SSN;
		else return ARConstants.ERROR_SSN;
	}
}
