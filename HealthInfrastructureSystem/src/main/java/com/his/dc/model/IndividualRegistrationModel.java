package com.his.dc.model;

import lombok.Data;

@Data
public class IndividualRegistrationModel {
	private Integer caseNo;
	private String firstname;
	private String lastname;
	private String ssn;
	private String dob;
	private String gender;
}
