package com.his.dc.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

import lombok.Data;

/**
*This is a demo class. It will be developed by a separate team.
*Need this class only to establish  caseNo as ForeignKey with IndividualRegistrationEntity
*/


@Table(name="DC_CASES")
@Entity
@Data
public class DcCasesEntity {

	@Id
	@GeneratedValue
	@Column(name="CASE_NO")
	private int caseNo;
	
	@Column(name="HOH_NAME")
	private String hohName;
	
	@CreationTimestamp
	@Column(name = "CREATE_DT")
	private Timestamp createdOn;
	
	@Column(name="EMAIL")
	private String email;
	
}
