package com.his.dc.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name="DC_INDV")
@Data
public class IndividualRegistrationEntity {
	@Id
	@Column(name="INDV_ID")
	@GeneratedValue
	private int indvId;
	
	@Column(name="FIRSTNAME")
	private String firstname;
	
	@Column(name="LASTNAME")
	private String lastname;
	
	@Column(name="SSN")
	private String ssn;
	
	@Column(name="DOB")
	private String dob;
	
	@Column(name="GENDER")
	private String gender;
	
	@ManyToOne
	@JoinColumn(name = "CASE_NO", referencedColumnName = "CASE_NO")
	private DcCasesEntity dcCaseNo;
}
