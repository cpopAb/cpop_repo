package com.his.dc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.his.dc.dao.CasesRegistrationDao;
import com.his.dc.dao.IndividualRegistrationDao;
import com.his.dc.entity.DcCasesEntity;
import com.his.dc.entity.IndividualRegistrationEntity;
import com.his.dc.indv.types.IndvDetailRequest;
import com.his.dc.indv.types.IndvDetailResponse;
import com.his.dc.model.IndividualRegistrationModel;
import com.his.dc.ws.SOAPConnector;
import com.his.util.ARConstants;

@Service("indvRegService")
public class IndividualRegistrationServiceImpl implements IndividualRegistrationService {

	@Autowired 
	private IndividualRegistrationDao indvDao;
	
	@Autowired
	private CasesRegistrationDao caseDao;
	
	@Autowired
	SOAPConnector soapConnector;
	
	@Override
	public IndividualRegistrationEntity registerIndiv(IndividualRegistrationModel indvRegModel) {
		System.out.println("IndividualRegistrationServiceImpl.registerIndiv()");
		IndividualRegistrationEntity savedEntity=null;

		//convert model to entity
		IndividualRegistrationEntity indvRegEntity=new IndividualRegistrationEntity();
		indvRegEntity.setFirstname(indvRegModel.getFirstname());
		indvRegEntity.setLastname(indvRegModel.getLastname());
		indvRegEntity.setGender(indvRegModel.getGender());
		indvRegEntity.setSsn(indvRegModel.getSsn());
		indvRegEntity.setDob(indvRegModel.getDob());
		
		//get DCCasesEntity object from DC_Cases table to store it as foreign key in DC_INDV table
		DcCasesEntity dcCasesEntity=caseDao.findById(indvRegModel.getCaseNo()).get();
		
		//set foreign key value
		indvRegEntity.setDcCaseNo(dcCasesEntity);
		System.out.println(indvRegEntity);
		savedEntity=indvDao.save(indvRegEntity);
		return savedEntity;
	}
	
	/**
	 * method is used to verify ssn and dob by calling the federal webservice endpoint
	 */
	@Override
	public boolean verifySsn(IndividualRegistrationModel indvRegModel) {
		System.out.println("verifySsn()");
		//create IndvDetailRequest object and set dob and ssn
		IndvDetailRequest request=new IndvDetailRequest();
		request.setDob(indvRegModel.getDob());
		request.setSsn(indvRegModel.getSsn());
		
		//call Federal web service which return Object class object
		IndvDetailResponse response=(IndvDetailResponse)soapConnector.callWebService(ARConstants.URL, request);
		try {
			//checking if firstname and dob are matching for the the given ssn 
			if((response.getIndvDetail().getFirstName().equals(indvRegModel.getFirstname()))&&
								(response.getIndvDetail().getDob().equals(indvRegModel.getDob()))){
				return true;
			}else return false;
		}catch(NullPointerException ne) {
			System.out.println(ne.getMessage());
			return false;
		}
	}

	/**
	 * method to get caseNo for the logged in email id
	 */
	@Override
	public Integer getCaseNo(String email) {
		DcCasesEntity entity=caseDao.findByEmail(email);
		if(entity!=null)
			return entity.getCaseNo();
		else
			return null;
	}

}
