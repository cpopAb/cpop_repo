package com.his.dc.service;

import com.his.dc.entity.IndividualRegistrationEntity;
import com.his.dc.model.IndividualRegistrationModel;

public interface IndividualRegistrationService {
	public Integer getCaseNo(String email);
	public IndividualRegistrationEntity registerIndiv(IndividualRegistrationModel indivRegModel);
	public boolean verifySsn(IndividualRegistrationModel indvRegModel); 
}
