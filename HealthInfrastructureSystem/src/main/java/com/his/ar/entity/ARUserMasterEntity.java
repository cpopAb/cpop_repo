package com.his.ar.entity;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.Data;

@Table(name="AR_USER_MASTER")
@Entity
@Data
public class ARUserMasterEntity {
	@Id
	@Column(name="AR_USER_ID")
	@GeneratedValue
	private Integer userId;
	
	@Column(name="FIRST_NAME")
	private String firstname;
	
	@Column(name="LAST_NAME")
	private String lastname;
	
	@Column(name="USER_EMAIL")
	private String email;
	
	@Column(name="USER_PWD")
	private String pwd;
	
	@Column(name="USER_DOB")
	private Date dob;
	
	@Column(name="USER_PHNO")
	private String phonenumber;
	
	@Column(name="USER_ROLE")
	private String role;
	
	@Column(name="ACTIVE_SW")
	private String  activeSwitch;
	
	@CreationTimestamp
	@Column(name="CREATED_DT")
	private Timestamp createdOn;
	
	@UpdateTimestamp
	@Column(name="UPDATED_DT")
	private Timestamp updatedOn;
	
	@Column(name="CREATED_BY")
	private String createdBy;
	
	@Column(name="UPDATED_BY")
	private String updatedBy;
	
}
