package com.his.ar.model;

import java.sql.Timestamp;

import lombok.Data;
  
@Data
public class ARUserMasterModel {
	
	private Integer userId;
	private String firstname;
	private String lastname;
	private String email;
	private String pwd;
	private String dob;
	private String phonenumber;
	private String role;
	private String activeSwitch;
	private Timestamp createdOn;
	private Timestamp updatedOn;
	private String createdBy;
	private String updatedBy;
	
	
}
