package com.his.ar.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.his.ar.entity.ARUserMasterEntity;
import com.his.ar.model.ARUserMasterModel;
import com.his.ar.service.ARService;
import com.his.util.ARConstants;

/**
 * Application Registration Module Controller 
 * @author abinashbiswal058
 *
 */
@Controller
@SessionAttributes({"email","caseNo"})
public class ARController {
	private static Logger logger=LoggerFactory.getLogger(ARController.class);
	
	@Autowired(required=true)
	private ARService arService;
	

	
	/**
	 * this method shows the login form
	 * @param model
	 * @return
	 */
	@RequestMapping({ "/", "/index" })
	public String index(@ModelAttribute("login") ARUserMasterModel user,Model model) {
		logger.info("Login Page shown");
		model.addAttribute("um", new ARUserMasterModel());
		return "index";
	}
	
	
	/**
	 * This method checks the login credentials
	 * @param am
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String AdminAndCaseWorker(@ModelAttribute("login") ARUserMasterModel user, Model model) {
		String view = "index";
		return authorized(user, model, view);

	}

	/** Authorized method of admin and case worker */
	private String authorized(ARUserMasterModel user, Model model, String view) {
		ARUserMasterModel um = arService.findUserByEmailAndPwd(user.getEmail(), user.getPwd());
		if (um == null) {
			
			model.addAttribute(ARConstants.ERROR, ARConstants.INVALID_CREDENTIALS);
		} else {
			if (ARConstants.ROLE.equals(um.getRole())) {
				return "dashboardAdmin";
			} else {
				if (ARConstants.ACTIVE.equals(um.getActiveSwitch())) {
					model.addAttribute("email", user.getEmail());//added email in session attribute
					return "dashboardCaseWorker";
				} else {
					model.addAttribute(ARConstants.ERROR, ARConstants.DEACTIVATE);
				}
			}
		}
		return view;
	}

	/**
	 * this method is used for LogOut admin or case worker user
	 * 
	 * @param session
	 * @return
	 */
 
	@RequestMapping(value = "/logout")
	public String forGetout(SessionStatus status) {
		status.setComplete();
		return "redirect:/index?ac=lo";
	}

	
	
	 /**
	  * This method shows the registration form for caseworker
	  * @param model
	  * @return String
	  */
	@RequestMapping(name="/registerUser",method=RequestMethod.GET)
	public String showRegistrationForm(Model model) {
		logger.debug("started case worker registration  form display");
		model.addAttribute("um", new ARUserMasterModel());
		initForm(model);
		logger.debug("ended case worker registration form display");
		return "caseworkerReg";
	}

	/**
	 * This method is used to save caseWorker profile
	 * @param aumodel
	 * @param model
	 * @return String
	 * @throws IOException
	 */
	@RequestMapping(name="/registerUser", method=RequestMethod.POST)
	public String registerUser( @ModelAttribute("um") ARUserMasterModel aumodel, Model model ) throws IOException {
		logger.debug("CaseWorker registration started");
		//call indvService method to save caseWorker profile
		ARUserMasterModel master =arService.saveUser(aumodel);

		if (master.getUserId()!=null) {
			logger.info("CaseWorker Registration Succesfull");
			model.addAttribute(ARConstants.SUCCESS, ARConstants.SUCCESS_MSG);
			model.addAttribute("um", new ARUserMasterModel());
		}else {
			logger.error("CaseWorker Registration Failed");
			model.addAttribute(ARConstants.ERROR,ARConstants.ERROR_MSG);
		}
		logger.debug("CaseWorker regiostration ended");
		return "caseworkerReg";
	}
	
	/**
	 * This method checks uniqueness for the user email id.
	 * @param email
	 * @return
	 */
	@RequestMapping("/checkEmail")
	public @ResponseBody String checkUserEmail(@RequestParam("email") String email) {
		logger.debug("Verifying CaseWorker Email");
		return arService.checkUserEmail(email);
	}
	
	/**
	 * This method returns list of options for Roles
	 * @param model
	 */
	private void initForm(Model model) {
		logger.debug("Option List for Roles generation started");
		List<String> list=new ArrayList<>();
		list.add("Admin");
		list.add("Case Worker");
		logger.debug("Option List for Roles generation ended");
		model.addAttribute("roles", list);
	}
	
	/**
	 * This method shows the forgot password form.
	 * @return
	 */
	@RequestMapping("/forgotPwd")
	public String showForgotPwdForm() {
		logger.info("Forgot Password Page shown");
		return "forgotPwdForm";
	}
	
	@RequestMapping("/viewCaseWorker")
	public String showCaseWorker(@RequestParam(value="cpn", defaultValue="1") String pageNo,Model model) {
		Integer pageNumber=Integer.parseInt(pageNo);
		//call indvService method
		Page<ARUserMasterEntity> page=arService.findAllUsers(pageNumber-1,ARConstants.PAGE_SIZE);
		
		//get pageContent as entity
		List<ARUserMasterEntity> listEntity=page.getContent();
		
		//get total no of pages
		int totalPages=page.getTotalPages();
		
		//convert listEntity to listModel
		List<ARUserMasterModel> listModel=new ArrayList<>();
		for(ARUserMasterEntity entity:listEntity) {
			ARUserMasterModel arModel=new ARUserMasterModel();
			BeanUtils.copyProperties(entity, arModel);
			listModel.add(arModel);
		}
		//create model object
		model.addAttribute("caseWorkers", listModel);
		model.addAttribute("cpn", pageNo);
		model.addAttribute("totalPages", totalPages);
		return "viewCaseWorker";
	}
	
	@RequestMapping("/activeCaseWorker")
	public String activateCWProfile(@RequestParam("uid") String userId) {
		
		if(!(userId==null||userId.equals(""))) {
			Integer uid=Integer.parseInt(userId);
			//call indvService method
			ARUserMasterModel umModel=arService.findById(uid);
			umModel.setActiveSwitch(ARConstants.STR_Y);
			ARUserMasterEntity updatedUmEntity=arService.update(umModel);
			if(updatedUmEntity!=null) {
				logger.info("Active Switch Details(activate) updated");
			}else {
				logger.info("Active Switch Details(activate) failed to update");
			}
		}
		return "redirect:viewCaseWorker";
	}
	
	@RequestMapping("/deleteCaseWorker")
	public String deleteCWProfile(@RequestParam("uid") String userId) {
		
		if(!(userId==null||userId.equals(""))) {
			Integer uid=Integer.parseInt(userId);
			System.out.println(uid);
			//call indvService method
			ARUserMasterModel umModel=arService.findById(uid);
			umModel.setActiveSwitch(ARConstants.STR_N);
			ARUserMasterEntity updatedUmEntity=arService.update(umModel);
			if(updatedUmEntity!=null) {
				logger.info("Active Switch Details(delete) updated");
			}else {
				logger.info("Active Switch Details(delete) failed to update");
			}
		}
		return "redirect:viewCaseWorker";
	}

}
