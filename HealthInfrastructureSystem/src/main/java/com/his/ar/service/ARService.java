package com.his.ar.service;

import java.io.IOException;
import java.util.List;

import org.springframework.data.domain.Page;

import com.his.ar.entity.ARUserMasterEntity;
import com.his.ar.model.ARUserMasterModel;

public interface ARService {
	public ARUserMasterModel findUserByEmailAndPwd(String email,String pwd);
	public ARUserMasterModel saveUser(ARUserMasterModel am) throws IOException;
	public List<ARUserMasterModel> findAllUser();
	public Page<ARUserMasterEntity> findAllUsers(Integer pageNo,Integer pageSize);
	public ARUserMasterModel findById(Integer userId);
	public ARUserMasterEntity update(ARUserMasterModel am);
	public String checkUserEmail(String email);
	
}
