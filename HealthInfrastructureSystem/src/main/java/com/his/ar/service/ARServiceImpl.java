package com.his.ar.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.his.ar.dao.ARUserMasterDao;
import com.his.ar.entity.ARUserMasterEntity;
import com.his.ar.model.ARUserMasterModel;
import com.his.util.ARConstants;
import com.his.util.EmailBody;
import com.his.util.MailService;
import com.his.util.PasswordUtils;

/**
 * This class is used to handle business operations in UserManagement module
 * @author user
 *
 */
@Service("arServiceImpl")
public class ARServiceImpl implements ARService {
	
	@Autowired(required=true) //required:without creating dao object donot create service object
	private ARUserMasterDao arUserMasterDao;
	@Autowired(required=true)
	private MailService mailService;
	
	/**
	 * This method is used to check login credentials and return model object.
	 * Entity object is returned from DAO which is converted to model object and returned to
	 * Controller.
	 */
	@Override
	public ARUserMasterModel findUserByEmailAndPwd(String email, String pwd) {
		ARUserMasterEntity entity=null;
		ARUserMasterModel model=null;
		
		//call dao method to check for login credentials
		entity=arUserMasterDao.findUserByEmailAndPwd(email, pwd);
		
		//copy entity object to model object
		model=new ARUserMasterModel();
		BeanUtils.copyProperties(entity, model);	
		return model;
		
	}
	/**
	 * method to save caseworker profile
	 * @throws IOException 
	 */
	@Override
	public ARUserMasterModel saveUser(ARUserMasterModel am)  {
		ARUserMasterEntity entity,savedEntity=null;
		boolean result=false;
		//create entity
		entity=new ARUserMasterEntity();
		
		//transfer data from model to entity
		BeanUtils.copyProperties(am, entity);
		
		//encrypt user pwd before saving the entity
		PasswordUtils.generateSecurePassword(am.getPwd());
		
		//set default values for activeSwitch and createdBy
		entity.setActiveSwitch(ARConstants.SWITCH);
		entity.setCreatedBy(ARConstants.ADMIN);
		
		//calling dao method to save the caseWorker profile
		savedEntity=arUserMasterDao.save(entity);
		
		//logic to send password to given email
		if(savedEntity!=null) {
			try {
				result=mailService.sendEmail(am.getEmail(), ARConstants.MAIL_FROM,
							  		   ARConstants.MAIL_SUBJECT, EmailBody.getCWRegEmailBody(am));
			} catch (IOException e) {
				
				e.printStackTrace();
			}
		}
		if(result) {
			//set Id in model
			am.setUserId(entity.getUserId());
			return am;
		}else return null;
			
	}

	/**
	 * this method is used to retrieve  all caseWorkers profile 
	 */
	@Override
	public List<ARUserMasterModel> findAllUser() {
		List<ARUserMasterEntity> listARUserMasterEntity=arUserMasterDao.findAll();
		List<ARUserMasterModel> listModel=new ArrayList<ARUserMasterModel>();
		//convert Entity to Model
		for(ARUserMasterEntity entity:listARUserMasterEntity) {
			ARUserMasterModel model=new ARUserMasterModel();
			BeanUtils.copyProperties(entity, model);
			listModel.add(model);
		}
		return listModel;
	}
	@Override
	public Page<ARUserMasterEntity> findAllUsers(Integer pageNo,Integer pageSize) {
		Pageable pageable=new PageRequest(pageNo, pageSize);	
		Page<ARUserMasterEntity> page=arUserMasterDao.findAll(pageable);
		return page;
	}

	/**
	 * this method is used to retrieve a caseworker by its ID 
	 */
	@Override
	public ARUserMasterModel findById(Integer userId) {
		//load entity object using id
		ARUserMasterEntity entity=arUserMasterDao.findById(userId).get();
		ARUserMasterModel model=new ARUserMasterModel();
		BeanUtils.copyProperties(entity, model);
		return model;
	}

	/**
	 * This method is used to update/delete a caseWorker
	 */
	@Override
	public ARUserMasterEntity update(ARUserMasterModel am) {
		ARUserMasterEntity entity=new ARUserMasterEntity();
		BeanUtils.copyProperties(am, entity);
		ARUserMasterEntity savedEntity=arUserMasterDao.save(entity);
		return	savedEntity;
	}
	
	/**
	 * this method is used to to know uniqueness of the passed email
	 */
	@Override
	public String checkUserEmail(String email) {
		Integer result=arUserMasterDao.findByEmail(email);
		return (result>=1)?ARConstants.DUPLICATE_EMAIL:ARConstants.UNIQUE_EMAIL;
	}
	

}
