package com.his.ar.dao;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.his.ar.entity.ARUserMasterEntity;

/**
 * This class is used for persistence operation
 * 
 * @author user
 *
 */
@Repository("arUserMasterDao")
public interface ARUserMasterDao extends JpaRepository<ARUserMasterEntity, Serializable> {
	/**
	 * This is a user defined method to check login credentials
	 * @param email
	 * @param pwd
	 * @param activeSw
	 * @return
	 */
	@Query("from ARUserMasterEntity ar where ar.email=:email and ar.pwd=:pwd")
	public ARUserMasterEntity findUserByEmailAndPwd(String email, String pwd);
	
	/**
	 * This is a user defined method to find the no. of emails with given email
	 * id in db
	 * @param email
	 * @return
	 */
	@Query("select count(*) from ARUserMasterEntity a where a.email=:email")
	public Integer findByEmail(String email);
}
