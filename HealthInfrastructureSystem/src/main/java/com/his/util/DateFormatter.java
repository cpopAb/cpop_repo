package com.his.util;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class DateFormatter {
	
	public static Date stringToSQLDate(String date) {
		SimpleDateFormat sdf=null;
		Date sqlDate=null;
		
		sdf=new SimpleDateFormat("dd-MM-yyyy");
		try {
			sqlDate = new Date((sdf.parse(date)).getTime());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return sqlDate;
	}

}
