package com.his.util;

public class ARConstants {
	public static final String STR_Y="Y";
	public static final String STR_N="N";

	//invalid login credentials
	public static final String INVALID_CREDENTIALS="Wrong Credentials";
	public static final String ACTIVE = "Y";
	public static final String ROLE = "Admin";
	public static final String ROLE_CASEWORKER = "Case Worker";
	public static final String DEACTIVATE = "Your Account is De-Activated";
	//registration msgs
	public static final String SUCCESS_MSG="Caseworker registerd successfully";
	public static final String SUCCESS="success";
	public static final String ERROR_MSG="Registration failed";
	public static final String ERROR="error";
	
	//constants for mailService
	public static final String MAIL_FROM="jakartaee.developer@gmail.com";
	public static final String MAIL_SUBJECT="HIS App Registration Successful";
	public static final String DUPLICATE_EMAIL="DUPLICATE";
	public static final String UNIQUE_EMAIL="UNIQUE";
	
	//constants for default values for entity
	public static final String ADMIN="ADMIN";
	public static final String SWITCH="Y";
	public static final Integer PAGE_SIZE=2;
	
	//constants for dc webservices
	public static final String URL="http://localhost:9090/service/ssn-details";
	public static final String BINDING_CLASS_PKG="com.his.dc.indv.types";
	
	//constants for dc module
	public static final String DC_REGISTSER_MSG="member added successfully";
	public static final String DC_NOT_REGISTSER_MSG="member not added";
	public static final String DC_REGISTER="register";
	public static final String DC_NOT_REGISTSER="error";
	public static final String ERROR_SSN="INVALID";
	public static final String SUCCESS_SSN="VALID";
	
}
