package com.his.util;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service("mailService")
public class MailService {
	
	@Autowired(required=true)
	private JavaMailSender mailSender;
	
	public boolean sendEmail(String to,String from,String subject,String body) {
		boolean flag=false;
		//create MimeMessage
		MimeMessage message= mailSender.createMimeMessage();
		MimeMessageHelper helper=new MimeMessageHelper(message);
		
		try {
			helper.setTo(to);
			helper.setFrom(from);
			helper.setSubject(subject);
			helper.setText(body, true);
			mailSender.send(message);
			flag=true;
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		
 		return flag;
	}
}
