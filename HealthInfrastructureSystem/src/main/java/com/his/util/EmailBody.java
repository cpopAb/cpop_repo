package com.his.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import com.his.ar.model.ARUserMasterModel;

public class EmailBody {
	
	public static String  getCWRegEmailBody(ARUserMasterModel am) throws IOException {
		FileReader fr=null;
		BufferedReader br=null;
		String line=null;
		StringBuilder mailBody=null;
				
			String fileName="RegSuccessEmailTemplate.txt";
			fr=new FileReader(fileName);
			br=new BufferedReader(fr);
			mailBody=new StringBuilder("");
			line=br.readLine();
			while(line!=null) {
				//process to replace placeholders
				if(line.contains("USER_FIRST_NAME"))
					line=line.replace("USER_FIRST_NAME", am.getFirstname());
				if(line.contains("USER_MAIL"))
					line=line.replace("USER_MAIL", am.getEmail());
				if(line.contains("USER_PASSWORD"))
					line=line.replace("USER_PASSWORD", am.getPwd());
				//append the each String line to a StringBuilder obj
				mailBody.append(line);
				//read next line(to avoid infinite looping)
				line=br.readLine();
			}
			br.close();
			fr.close();
		
		return mailBody.toString();
	}

}
