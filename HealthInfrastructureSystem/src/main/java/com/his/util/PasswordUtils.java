package com.his.util;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.Base64;
import java.util.Random;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

public final class PasswordUtils {

	private static final Random RANDOM = new SecureRandom();
	private static final String ALPHABET = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	private static final int ITERATIONS = 10000;
	private static final int KEY_LENGTH = 256;

	/**
	 * This method is used to generate salts used for password encryption
	 * @return
	 */
	public static String getSalt() {
		int length=30;
		StringBuilder returnValue = new StringBuilder(length);
		for (int i = 0; i < length; i++) {
			returnValue.append(ALPHABET.charAt(RANDOM.nextInt(ALPHABET.length())));
		}
		return new String(returnValue);
	}

	/**
	 * This method has hashing algorithm
	 * @param password
	 * @param salt
	 * @return
	 */
	public static byte[] hash(final char[] password,final byte[] salt) {
		final PBEKeySpec spec = new PBEKeySpec(password, salt, ITERATIONS, KEY_LENGTH);
		Arrays.fill(password, Character.MIN_VALUE);
		try {
			SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
			return skf.generateSecret(spec).getEncoded();
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			throw new AssertionError("Error while hashing a password: " + e.getMessage(), e);
		} finally {
			spec.clearPassword();
		}
	}
	
	/**
	 * this method generates Secure Password using hashing and salting.
	 * before returning the pwd,it is encrypted.
	 * @param password
	 * @param salt
	 * @return
	 */
	public static String generateSecurePassword(final String password, final String salt) {

		final byte[] securePassword = hash(password.toCharArray(), salt.getBytes());
		return Base64.getEncoder().encodeToString(securePassword);
		 
	}
	/**
	 * this method generates Secure Password using hashing and salting.
	 * before returning the pwd,it is encrypted.
	 * @param password
	 * @param salt
	 * @return
	 */
	public static String generateSecurePassword(final String password) {
		byte[] securePassword =null;
		final String salt=getSalt();
		securePassword = hash(password.toCharArray(), salt.getBytes());
		return Base64.getEncoder().encodeToString(securePassword);

	}
	/**
	 * THis method verifies entered password with saved password
	 * @param providedPassword
	 * @param securedPassword
	 * @param salt
	 * @return
	 */
	public static boolean verifyUserPassword(final String providedPassword, final String securedPassword, final String salt) {
		
		// Generate New secure password with the same salt
		final String newSecurePassword = generateSecurePassword(providedPassword, salt);

		// Check if two passwords are equal
		return newSecurePassword.equalsIgnoreCase(securedPassword);

	}
}

