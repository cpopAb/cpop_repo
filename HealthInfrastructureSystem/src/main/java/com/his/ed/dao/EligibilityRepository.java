package com.his.ed.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.his.ed.entity.EligibilityDetailEntity;

@Repository
public interface EligibilityRepository {

	@Query("from EligibilityDetailEntity where caseNum=:caseNum")
	public EligibilityDetailEntity findByCaseNumber(Long caseNum);
	
	
}
