package com.his.ed.service;

import com.his.ed.model.EligibilityDetailModel;

public interface EligibilityDetailService {

	public EligibilityDetailModel findByCaseNum(Long caseNum);
}
