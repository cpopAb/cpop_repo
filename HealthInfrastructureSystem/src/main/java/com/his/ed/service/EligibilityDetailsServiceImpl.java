package com.his.ed.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.his.ed.dao.EligibilityRepository;
import com.his.ed.entity.EligibilityDetailEntity;
import com.his.ed.model.EligibilityDetailModel;

@Service
public class EligibilityDetailsServiceImpl implements EligibilityDetailService {

	@Autowired
	private EligibilityRepository eligRepository;
	@Override
	public EligibilityDetailModel findByCaseNum(Long caseNum) {
		EligibilityDetailEntity eligEntity=eligRepository.findByCaseNumber(caseNum);
		//convert entity to model
		EligibilityDetailModel eligModel=new EligibilityDetailModel();
		BeanUtils.copyProperties(eligEntity, eligModel);
		return eligModel;
	}

}
