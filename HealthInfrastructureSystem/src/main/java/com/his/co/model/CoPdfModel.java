package com.his.co.model;

import lombok.Data;

@Data
public class CoPdfModel {
	private Integer coPdfId;
	private String caseNumber;
	private String pdfDocument;
	private String planName;
	private String PlanStatus;

}
