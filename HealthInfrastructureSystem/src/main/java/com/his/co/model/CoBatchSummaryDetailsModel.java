package com.his.co.model;

import lombok.Data;

@Data
public class CoBatchSummaryDetailsModel {
	private Integer summaryId;
	private String batchName;
	private Integer totalTriggerProcessed;
	private Integer failureTriggerCount;
	private Integer successTriggerCount;
}
