package com.his.co.dao;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.his.co.entity.CoBatchSummaryEntity;

@Repository
public interface CoBatchSummary extends JpaRepository<CoBatchSummaryEntity, Serializable> {

}
